
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/models/user';
import { ServiceUtils } from './service.utils';
import { FileUploader } from 'ng2-file-upload';
import { Form } from '@angular/forms';


@Injectable({ providedIn: 'root' })
export class UploadService {
    
    constructor(private http: HttpClient, private serviceUtils: ServiceUtils) { }
    apiUrl = "http://localhost:8091/api";  //Need to get this url

    uploadFile(form: FormData) {
        //this.http.post(`${this.apiUrl}/fileUpload`, form, {headers:this.serviceUtils.createTokenHeader()});
        console.log("here trying to upload")
        return this.http.get<User[]>(`${this.apiUrl}/users`, this.getRequestHeaders());
    }
    getRequestHeaders(){
        return {headers: this.serviceUtils.createTokenHeader()};
    }
}