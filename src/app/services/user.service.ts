import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/models/user';
import { ServiceUtils } from './service.utils';
import { FileUploader } from 'ng2-file-upload';


@Injectable({ providedIn: 'root' })
export class UserService {
    
    constructor(private http: HttpClient, private serviceUtils: ServiceUtils) { }
    apiUrl = "http://localhost:8091/api";  //Need to get this url

    getAll() {
        return this.http.get<User[]>(`${this.apiUrl}/users`, this.getRequestHeaders());
    }

    getById(id: number) {
        return this.http.get(`${this.apiUrl}/users/${id}`);
    }

    register(user: User) {
        return this.http.post(`${this.apiUrl}/users/register`, user);
    }

    update(user: User) {
        return this.http.put(`${this.apiUrl}/users/${user.id}`, user);
    }

    delete(id: number) {
        return this.http.delete(`${this.apiUrl}/users/${id}`);
    }

    getRequestHeaders(){
        return {headers: this.serviceUtils.createTokenHeader()};
    }
}