import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ServiceUtils {
    public createTokenHeader(): HttpHeaders {
        let reqOptions = new HttpHeaders().set( 'Content-Type', 'application/x-www-form-urlencoded' )
        if(this.token) {
            reqOptions = new HttpHeaders().set( 'Content-Type', 'application/x-www-form-urlencoded' ).set('Authorization', 'Bearer ' + this.token);
        }
        return reqOptions;
    }

    get token():string{
        return !localStorage.getItem("token") ? null : localStorage.getItem("token");
    }
}