import { Component, OnInit } from '@angular/core';
import { UploadService } from '../services/upload.service';



@Component({
  selector: 'app-point-upload',
  templateUrl: './point-upload.component.html',
  styleUrls: ['./point-upload.component.scss'],
  
})
export class PointUploadComponent implements OnInit {

  apiUrl = "http://localhost:8091/api"
  
  
  constructor(private uploadService: UploadService) { }

  ngOnInit() {
  }

  
  uploadFile(data){
    let form = new FormData();
    form.append("file", data.target.files[0]);
    this.uploadService.uploadFile(form);
  }
 

}
